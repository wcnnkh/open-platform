# open-platform
对第三方开放平台的整合

使用方式:

1.pom.xml中引入仓库
-------------------
	<repositories>
		  <repository>
			  <id>scw</id>
			  <name>scw</name>
			  <url>http://maven.shuchaowen.com</url>
		  </repository>
    </repositories>
或下载源码打包(自己打包需要先打包父项目[https://github.com/wcnnkh/scw](https://github.com/wcnnkh/scw))

---
# 支付
微信<br/>
支付宝<br/>
apple<br/>

---
# 物流
快递鸟<br/>

---
# 授权
微信<br/>
微信小程序<br/>
QQ互联<br/>
抖音<br/>

---
# 资源存储
aliyun oss<br/>

---
# SMS
阿里云短信服务(原阿里大鱼)<br/>