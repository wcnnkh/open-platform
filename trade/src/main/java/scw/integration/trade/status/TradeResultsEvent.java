package scw.integration.trade.status;

import scw.event.Event;
import scw.integration.trade.TradeResults;

public class TradeResultsEvent extends TradeResults implements Event {
	private static final long serialVersionUID = 1L;
}
